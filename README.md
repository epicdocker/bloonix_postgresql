<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
	* 1.1. [Details](#Details)
* 2. [Links](#Links)
* 3. [License](#License)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


# Pre-initialized PostgreSQL database for Bloonix

On the [recommendation](https://bloonix-monitoring.org/en/docs/installation/postgresql.html) of Bloonix, PostgreSQL version 9 is used. This Docker image is for demo and small installations.

WARNING: The Init-SQL script sets a default password and must be changed after the first login.


##  1. <a name='Versions'></a>Versions

The version numbers are separate versions and have no connection with the used applications in this Docker image. In the description you can see the version numbers of the integrated applications.

`latest` [Dockerfile](https://gitlab.com/epicdocker/bloonix_postgresql/blob/master/Dockerfile)

`1.0.0` [Dockerfile](https://gitlab.com/epicdocker/bloonix_postgresql/blob/release-1.0.0/Dockerfile)


###  1.1. <a name='Details'></a>Details

`1.0.0` `latest`

- Used the official [postgres](https://hub.docker.com/_/postgres/) image in version `9.6.6-alpine`.
- Extended to the initialization SQL script of the Bloonix webgui in version `0.131-1`.


##  2. <a name='Links'></a>Links 

- Bloonix server Docker image - https://gitlab.com/epicdocker/bloonix_server
- Bloonix agent Docker image - https://gitlab.com/epicdocker/bloonix_agent
- Bloonix satellite Docker image - https://gitlab.com/epicdocker/bloonix_satellite
- Source Code - https://gitlab.com/epicdocker/bloonix_postgresql
- GitLab Registry - https://gitlab.com/epicdocker/bloonix_postgresql/container_registry
- Docker Hub Registry - https://hub.docker.com/r/epicsoft/bloonix_postgresql/
- Bloonix OpenSource Monitoring Software - https://bloonix-monitoring.org/


##  3. <a name='License'></a>License

MIT License see [LICENSE](https://gitlab.com/epicdocker/bloonix_postgresql/blob/master/LICENSE)
