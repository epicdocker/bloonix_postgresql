FROM postgres:9.6.6-alpine
LABEL image.name="bloonix_postgresql" \
      image.description="Pre-initialized PostgreSQL database for Bloonix" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2018 epicsoft.de / Alexander Schwarz" \
      license="MIT"

COPY ["schema-pg.sql", "/docker-entrypoint-initdb.d/schema-pg.sql"]
